<?php
$components = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '83b635aa003c090a1f203e480c17279d',
        ],
    ]
];
return $components;