<?php

use yii\helpers\ArrayHelper;

$array = [
    'adminEmail' => 'admin@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'armaID' => '107410'
];
$array = ArrayHelper::merge(require (__DIR__ . '/params-local.php'), $array);
return $array;