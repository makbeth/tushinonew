<?php

use yii\helpers\ArrayHelper;

$array = [
    'class' => 'yii\db\Connection',
    'charset' => 'utf8',
    'tablePrefix' => 'tsg_',
    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];

$array = ArrayHelper::merge(require (__DIR__ . '/db-local.php'), $array);
return $array;