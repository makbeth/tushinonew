<?php


namespace app\assets;


use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $js = [
        'js/common/common_functions.js',
        'js/common/default.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}