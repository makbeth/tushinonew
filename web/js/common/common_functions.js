/**
 * Created by makbeth on 27.10.16.
 */
var common_functions = {

    //Получение любых связанных списков, данных и т.д. Пример: отобразить категории для типов в разделе материалов.
    getRelativeData: function(){
        bodyOffOn('change', '.relative-parent', function(e){

            var url = $(this).data('href');
            var params = {
                id       : $(this).val()
            };

            $('.relative-child-items').val('');
            $.post(url, params, function (data){
                if ($(data).length > 0){
                    if(data.status==true){
                        $('#relative-child').html(data.html);
                    }else{
                        alert('Что-то пошло не так. Подробные сообщения об ошибках появятся позже.')
                    }
                }

            }, 'json');

            if($(this).data('href2')!=null){

                 url = $(this).data('href2');
                 params = {
                    id       : $(this).val()
                };

                $.post(url, params, function (data){
                    if ($(data).length > 0){
                        if(data.status==true){
                            $('#relative-child-2').html(data.html);
                        }else{
                            alert('Что-то пошло не так. Подробные сообщения об ошибках появятся позже.')
                        }
                    }

                }, 'json');

            }

        });
    },

    maskInput: function(obj) {
        $('.js-mask-input', obj).not('.is-stl').each(function () {
            var maskInput = $(this);

            maskInput.inputmask().addClass('is-stl');
        });

    },


    init: function() {
        this.maskInput();
      this.getRelativeData();


    }
};

$(document).ready(function(){
    //init func
    common_functions.init();
});