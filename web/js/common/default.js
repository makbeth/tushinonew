/**
 * @description Alternative replacement code:
 * @description $('body').off(events, selector).on(events, selector, callback);
 * 
 * @param string events
 * @param string selector
 * @param mixed callback
 */
function bodyOffOn(events, selector, callback){
    $('body').off(events, selector).on(events, selector, callback); 
}

/**
 * @description Close popup and reload page
 * 
 * @param string selector
 */
function pageReload(selector){
    if(typeof(selector) !== 'undefined'){
        $(selector).remove();
    }
    window.location.reload();  
}

/**
 * @description Scrolling Page
 * 
 * @param string selector
 * @param int marginTop
 */
function scrollingPage(selector, marginTop){    
    var margin = ($(marginTop).length > 0) ? parseInt(marginTop) : 0;
    var top = $(selector).offset().top - margin;
    $('html, body').animate({scrollTop : top},'slow');
}

/**
 * @description Scrolling Box
 * 
 * @param string selector
 * @param string box
 * @param int marginTop
 */
function scrollingBox(selector, box, marginTop){    
    var margin = ($(marginTop).length > 0) ? parseInt(marginTop) : 0;
    var top = $(selector).offset().top + $(box).scrollTop() - $(box).offset().top - margin;
   
    $(box).animate({scrollTop : top},'fast');
}

/**
 * @description Set cursor wait
 * 
 * @param string|object selector
 */
function setCursorWait(selector){
    $('body').css('cursor', 'wait');
    $(selector).css('cursor', 'wait'); 
}

/**
 * @description Set cursor default
 * 
 * @param string|object selector
 */
function setCursorDefault(selector){
    $('body').css('cursor', 'default');
    $(selector).css('cursor', 'default'); 
}

/**
 * @description Set cursor Auto
 * 
 * @param string|object selector
 */
function setCursorAuto(selector){
    $('body').css('cursor', 'auto');
    $(selector).css('cursor', 'auto'); 
}

/**
 * Checkbox switcher
 * @param object elem
 * @param bool status
 */
function checkboxChecked(elem, status){
    if(status === true){
        $(elem).attr('checked', 'checked');
        elem.checked = true;
    }else{
        $(elem).removeAttr('checked');
        elem.checked = false;
    }
}

/**
 * generate image src string
 * @param string src
 * @return string
 */
function generateImageSrc(src){
    return (src + '?' + new Date().getTime())
}

/**
 * Get outerHTML
 */
jQuery.fn.outerHTML = function(s) {
    return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
};

/**
 * Add br in div
 * @param object elem
 */
function insertBR(elem){
    //remove tmp br
    removeTmpBR(elem);

    var docFragment = document.createDocumentFragment();

    //add a new line
    var newEle = document.createTextNode('\n');
    docFragment.appendChild(newEle);

    //add the br, or p, or something else
    newEle = document.createElement('br');
    docFragment.appendChild(newEle);

    //make the br replace selection
    var range = window.getSelection().getRangeAt(0);
    range.deleteContents();
    range.insertNode(docFragment);

    //add tmp br
    if($(elem).find('br.__tmp_insert_br__').length == 0){
        $(elem).append('<br class="__tmp_insert_br__">');
    }

    //create a new range
    range = document.createRange();
    range.setStartAfter(newEle);
    range.collapse(true);

    //make the cursor there
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range); 
}

/**
 * Remove tmp br
 * @param object elem
 */
function removeTmpBR(elem){
    //remove tmp br
    $(elem).find('br.__tmp_insert_br__').remove();
}

/**
 * Strip HTML tags
 * @param string selector
 * @param string allow Allow tags
 */
function stripTags(selector, allow){
    if($(selector).length > 0){
        setTimeout(function(){
          if(typeof(allow) === 'undefined' || allow === ''){
              $(selector).html($(selector).text());
          }else{
              $(selector).find('*:not('+allow+')').contents().unwrap();
              $(selector).find('*:not('+allow+')').remove();

              $(selector).html(
                    $(selector).html().replace(/(\n){2,}/g, '<br>')
                                      .replace(/\s{2,}/g, ' ')
              );
          }
        }, 50);
    }
}

/**
 * Show and hide loader
 */
var ajaxLoader = 0;
function showLoader(){
    if(ajaxLoader == 0){
        $('body').prepend('<div class="ajax-loader"></div>');
    }
    ajaxLoader++;
}
function hideLoader(){
    setTimeout(function(){        
        ajaxLoader--;
        if(ajaxLoader == 0){
            $('body > .ajax-loader').remove();
        }
    }, 500)
}