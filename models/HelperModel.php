<?php


namespace app\models;


use yii\db\ActiveRecord;

class HelperModel extends ActiveRecord
{
    /*
     * Отдаем имя таблицы в формате {{%users}} а получаем имя таблицы с префиксом
     */
    public static function getTableName($table)
    {
        return \Yii::$app->db->tablePrefix.str_replace(array('{{%','}}'),'',$table);
    }
}