<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m190409_075057_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(15),
            'email' => $this->string(50),
            'email_status' => $this->smallInteger(1),
            'is_active' => $this->smallInteger(1),
            'is_blocked' => $this->smallInteger(1),
            'server_access' => $this->smallInteger(1),
            'teamspeak_access' => $this->smallInteger(1),
            'vpn_access' => $this->smallInteger(1),
            'mantis_access' => $this->smallInteger(1),
            'steam_id' => $this->string(18),
            'steam_guid' => $this->string(32),
            'teamspeak_id' => $this->string(32),
            'avatar' => $this->string(),
            'password' => $this->string(32),
            'cookie_key' => $this->string(32),
            'last_login_date' => $this->dateTime(),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime(),
            'auth_key' => $this->string(),
            'password_reset_token' => $this->string(),
            'verification_token' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
