<?php

use app\models\HelperModel;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%pages}}`.
 */
class m190411_100648_create_pages_table extends Migration
{
    public $parentTable = '{{%user}}';
    public $table = '{{%pages}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pages}}', [
            'id' => $this->primaryKey(),
            //1 - новость, 2 - статья (правила, сроки и степени, и т.д.) плюс можно добавить еще типы, если это нужно. Константами.
            'type' => $this->smallInteger(1),
            //Категории для новостей и для статей. Для каждой свои. Определять в константах.
            'category' => $this->integer(),
            'title' => $this->string(),
            'text' => $this->text(),
            'author_id' => $this->integer(),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime(),
        ]);
        $this->addForeignKey($this->fkName($this->table), $this->tableName($this->table), 'author_id', $this->tableName($this->parentTable), 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->fkName($this->table), $this->table);
        $this->dropTable('{{%pages}}');
    }

    private function fkName($table)
    {
        return 'FK_'.$this->tableName($table);
    }

    private function tableName($table)
    {
        return HelperModel::gettableName($table);
    }
}
