<?php

use app\models\HelperModel;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%personal_data}}`.
 */
class m190409_083136_create_personal_data_table extends Migration
{
    public $parentTable = '{{%user}}';
    public $table = '{{%personal_data}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'fullname' => $this->string(45),
            'birthdate'=> $this->date(),
            'country' => $this->string(50),
            'city' => $this->string(50),
            'timezone' => $this->string(50),
            'skype' => $this->string(50),
            'youtube' => $this->string(50),
            'twitter' => $this->string(50),
            'vkontakte' => $this->string(50),
            'about' => $this->string(255)
        ]);

        $this->addForeignKey($this->fkName($this->table), $this->tableName($this->table), 'user_id', $this->tableName($this->parentTable), 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey($this->fkName($this->table), $this->table);
        $this->dropTable('{{%personal_data}}');
    }

    private function fkName($table)
    {
        return 'FK_'.$this->tableName($table);
    }

    private function tableName($table)
    {
        return HelperModel::gettableName($table);
    }
}
