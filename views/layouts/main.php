<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    /*
     * Меню. Есть возможность юзать статичные кнопки, как и выводить динамические разделы материалов, которые вносятся из админки.
     * Динамические материалы будут идти одним блоком. С помощью array_splice можно этот блок двигать влево и вправо по меню.
     */

    $itemsStatic =  [

        ['label' => 'About', 'url' => ['/about']],

    ];

    if (Yii::$app->user->isGuest) {
        array_push($itemsStatic,['label' => 'Войти', 'url' => ['/user/login']],['label' => 'Регистрация', 'url' => ['/user/registration']]);
    } else {
        array_push($itemsStatic,['label' => 'Выйти (' . Yii::$app->user->identity->name . ')',
                'url' => ['/user/logout'],
                'linkOptions' => ['data-method' => 'post']]
        );
    }

    array_splice($itemsStatic, 1, 0, \app\modules\main\models\Pages_model::getMainMenu());


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $itemsStatic,
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
