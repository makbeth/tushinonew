<?php

namespace app\modules\main\controllers;

use app\modules\main\models\User_model;
use LightOpenID;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\main\models\frontend\SignupForm;
use app\modules\main\models\frontend\ResendVerificationEmailForm;
use app\modules\main\models\frontend\VerifyEmailForm;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use app\modules\main\models\frontend\ResetPasswordForm;
use app\modules\main\models\frontend\PasswordResetRequestForm;
use app\modules\main\models\frontend\LoginForm;


class UserController extends BaseController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUserView(){
        return $this->render('view');
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'registration'],
                'rules' => [
                    [
                        'actions' => ['registration'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    /*
     * Регистрация с проверкой стимаккаунта и Армы на нем. Не очень красиво написано, но как есть.
     */
    public function actionRegistration(){


        $steamKey = Yii::$app->params['steamKey'];
        //Проверка на наличие стим id в сессии. Если нету - запускаем авторизацию в стиме. Если есть - продолжаем регистрацию.
        if(!Yii::$app->session->get('steam_id')){

            $openid = new LightOpenID(Yii::$app->params['registrationPath']);
            if(!$openid->mode) {
                $openid->identity = 'http://steamcommunity.com/openid/?l=russian';

                $this->redirect($openid->authUrl());

            } elseif ($openid->mode == 'cancel') {
                Yii::$app->session->setFlash('error', 'Пользователь прервал авторизацию в Steam');
                return $this->render('registration-error');
            } else {
                if($openid->validate()) {
                    $id = $openid->identity;
                    $ptn = "/^https:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
                    preg_match($ptn, $id, $matches);

                    $url = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=$steamKey&steamid=$matches[1]";
                    $json_object = $this->curl_get_contents($url);
                    $json_decoded = json_decode($json_object);
                    //Проверка на наличие армы, если есть - продолжаем регистрацию. Если нет - выдаем вьюху с ошибкой и удаляем из сессии данный steamID.
                    if($this->checkArma($json_decoded)){
                        if($guid = $this->generateGUID($matches[1])){
                            Yii::$app->session->set('steam_id', $matches[1]);
                            Yii::$app->session->set('guid', $guid);

                        }

                    }else{
                        Yii::$app->session->remove('steam_id');
                        Yii::$app->session->setFlash('error', 'У вас отсутствует игра Arma 3 на вашем Steam акаунте.');
                        return $this->render('registration-error');
                    }

                } else {
                    Yii::$app->session->setFlash('error', 'Пользователь не авторизован в Steam');
                    return $this->render('registration-error');
                }
            }
        }

        //Собственно сам процесс регистрации, либо вывода формы для регистрации.
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            //По хорошему тут надобы удалить steamID из сессии, но таким образом можно спалить челиков, которые будут пытаться создавать дубли с тем же стим акком.
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }else{

            return $this->render('registration', ['model'=>$model]);
        }


    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }
        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }
    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }
        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function checkArma($object)
    {


        if(isset($object->response->games) && is_array($object->response->games)){

            $id = Yii::$app->params['armaID'];
            foreach ($object->response->games as $value){
                if($value->appid == $id)
                    return true;
            }
        }

        return false;
    }

    public function generateGUID($steamID)
    {
        $ret = false;
        if(is_string($steamID)){
            $temp = '';
            for ($i = 0; $i < 8; $i++) {
                $temp .= chr($steamID & 0xFF);
                $steamID >>= 8;
            }
            $ret = md5('BE' . $temp);

        }
        return $ret;

    }

   public function curl_get_contents($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

}
