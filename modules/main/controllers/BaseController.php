<?php


namespace app\modules\main\controllers;


use Yii;
use yii\web\Controller;

class BaseController extends Controller
{

    /**
     * Access only ajax
     */
    public final function isAjax()
    {
        if (!(Yii::$app->request->isAjax)) {
            die('Access denied!');
        }
    }
}