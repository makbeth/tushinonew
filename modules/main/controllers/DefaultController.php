<?php

namespace app\modules\main\controllers;


use app\modules\main\models\Pages_model;
use app\modules\main\models\User_model;
use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use LightOpenID;
use yii\base\ErrorException;


/**
 * Default controller for the `main` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {



       return $this->render('index');

    }

    public function actionMaterials()
    {
        $id = Yii::$app->request->get('id');

        $model = Pages_model::find()->where('id=:id', [':id' => $id])->one();

        return $this->render('materials', compact('model'));
    }

    public function actionAbout()
    {

        return $this->render('about');
    }

    /*
     * Для получения через Ajax списка категорий для типа материала.
     */
    public function actionGetCategories()
    {
        $this->isAjax();
        $ret = ['status' => false];

        $id = Yii::$app->request->post('id');


        if (!empty($id) && is_numeric($id)) {
            $array = Pages_model::getCategoriesByType($id);

            if (is_array($array)) {
                $ret = [
                    'html' => Html::dropDownList('Pages_model[category]', key($array), $array, ['class' => 'form-control relative-child-items']),
                    'status' => true,
                ];
            }

        }else{
            $ret = [
                'html' => Html::dropDownList('', null, ['Все категории'], ['class' => 'form-control', 'disabled'=>'disabled']),
                'status' => true
            ];
        }
        echo Json::encode($ret);
    }
}
