<?php


namespace app\modules\main\models;


use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class Pages_model extends Pages
{
    const NEWS_ID = 1;
    const PAGES_ID = 2;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if(isset(Yii::$app->user->identity->id)){
                $this->author_id = Yii::$app->user->identity->id;
                return true;
            }else{
                Yii::$app->session->setFlash('error', 'Вы должны быть авторизованным пользователем');
                return false;
            }

        }
        return false;
    }

    public static function getTypeList()
    {
        return ArrayHelper::map(self::getCategoriesList(), 'id', 'title');
    }

    public static function getCategoriesList()
    {
        return [
            ['id' => self::NEWS_ID, 'title' => 'Новость', 'categories' => [
                1 => 'Обновление сборки',
                2 => 'Список миссий',
                3 => 'Объявления'
            ]],
            ['id' => self::PAGES_ID, 'title' => 'Статья', 'categories' => [
                11 => 'Правила проекта',
                12 => 'Требования к играм'
            ]],
        ];
    }

    public static function getCategoriesByType($type_id = null)
    {
        $ret = [];
        if (!empty($type_id) && $type_id != 0) {
            $array = ArrayHelper::index(self::getCategoriesList(), 'id');
            if (isset($array[$type_id]))
                $ret = ArrayHelper::getValue($array[$type_id], 'categories');
        }
        return $ret;
    }

    public static function getOnlyCategories()
    {
        $ret = [];
        $array = ArrayHelper::map(self::getCategoriesList(), 'id', 'categories');
        foreach ($array as $value) {
            foreach ($value as $k => $v) {
                $ret[$k] = $v;
            }
        }
        return $ret;
    }

    public static function getPagesByCategory($category)
    {
        return self::findAll(['category'=>$category]);
    }

    /*
     * Формирование списка элементов выпадающего меню. Формируется исходя из названий материалов.
     */
    public static function getDropdownElements($category)
    {
        $pages = self::getPagesByCategory($category);
        $ret = [];
        foreach ($pages as $value){
            $ret[] = ['label' => $value->title, 'url'=>['/materials/'.$value->id]];
        }
        return $ret;
    }

    /*
     * Метод позволяет выводить категории материалов как название элементов меню, а сами материалы как названия пунктов выпадающего списка.
     */
    public static function getMainMenu()
    {

        $ret = [];
        $array = self::getCategoriesByType(self::PAGES_ID);

        foreach ($array as $key => $value){
            //Если в категории нет маетриалов - пункт главного менб не выводим
            if(self::getDropdownElements($key)){
                $ret[$key] = ['label' => $value];
                foreach (self::getDropdownElements($key) as $val){
                    $ret[$key]['items'][] = $val;

                }
            }

        }

        return $ret;
    }


}