<?php

namespace app\modules\main\models;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $email_status
 * @property int $is_active
 * @property int $is_blocked
 * @property int $server_access
 * @property int $teamspeak_access
 * @property int $vpn_access
 * @property int $mantis_access
 * @property int $steam_id
 * @property int $steam_guid
 * @property string $teamspeak_id
 * @property string $avatar
 * @property string $password
 * @property string $cookie_key
 * @property string $last_login_date
 * @property string $create_date
 * @property string $update_date
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $verification_token
 *
 * @property PersonalData[] $personalDatas
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_status', 'is_active', 'is_blocked', 'server_access', 'teamspeak_access', 'vpn_access', 'mantis_access', 'steam_id', 'steam_guid'], 'integer'],
            [['last_login_date', 'create_date', 'update_date'], 'safe'],
            [['name'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 50],
            [['teamspeak_id', 'password', 'cookie_key'], 'string', 'max' => 32],
            [['avatar', 'auth_key', 'password_reset_token', 'verification_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'email_status' => 'Email Status',
            'is_active' => 'Is Active',
            'is_blocked' => 'Is Blocked',
            'server_access' => 'Server Access',
            'teamspeak_access' => 'Teamspeak Access',
            'vpn_access' => 'Vpn Access',
            'mantis_access' => 'Mantis Access',
            'steam_id' => 'Steam ID',
            'steam_guid' => 'Steam Guid',
            'teamspeak_id' => 'Teamspeak ID',
            'avatar' => 'Avatar',
            'password' => 'Password',
            'cookie_key' => 'Cookie Key',
            'last_login_date' => 'Last Login Date',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'verification_token' => 'Verification Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalDatas()
    {
        return $this->hasMany(PersonalData::className(), ['user_id' => 'id']);
    }
}
