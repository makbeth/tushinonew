<?php


namespace app\modules\main\models\frontend;

use Yii;
use yii\base\Model;
use app\modules\main\models\User_model;

class SignupForm extends Model
{

    public $name;
    public $email;
    public $password;
    public $password_repeat;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => 'Используйте только английские символы'],
            ['name', 'required'],
            ['name', 'unique', 'targetClass' => 'app\modules\main\models\User_model', 'message' => 'This name has already been taken.'],
            ['name', 'string', 'min' => 2, 'max' => 15],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 50],
            ['email', 'unique', 'targetClass' => 'app\modules\main\models\User_model', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }
    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $steam_id = Yii::$app->session->get('steam_id');
        $guid = Yii::$app->session->get('guid');
        if(!$steam_id || !$guid)
            return false;

        $user = new User_model();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->steam_id = $steam_id;
        $user->steam_guid = $guid;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        return $user->save() && $this->sendEmail($user);
    }
    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }

}