<?php

namespace app\modules\main\models;

use Yii;

/**
 * This is the model class for table "{{%pages}}".
 *
 * @property int $id
 * @property int $type
 * @property int $category
 * @property string $title
 * @property string $text
 * @property int $author_id
 * @property string $create_date
 * @property string $update_date
 *
 * @property User $author
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'category', 'author_id'], 'integer'],
            [['text'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['type', 'category', 'title', 'text'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип статьи',
            'category' => 'Категория',
            'title' => 'Название',
            'text' => 'Текст',
            'author_id' => 'Author ID',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
}
