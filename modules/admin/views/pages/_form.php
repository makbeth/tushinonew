<?php

\app\assets\MainAsset::register($this);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\main\models\Pages_model;

/* @var $this yii\web\View */
/* @var $model app\modules\main\models\Pages_model */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-model-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="filters-single">
        <h5><strong>Тип материала</strong></h5>
    <div class="type-select">
        <div class="selsgroup">
            <?= $form->field($model, 'type')->dropDownList(Pages_model::getTypeList(),
                ['prompt' => 'Выберите тип материала', 'class' => 'form-control relative-parent', 'data-href'=>'/default/get-categories'])->label(false) ?>
        </div>
    </div>
    </div>

    <div class="filters-single">
        <h5><strong>Категория</strong></h5>
        <div class="category-select">
            <div class="selsgroup" id="relative-child">
                <?if(!empty($model->type)): ?>
                    <?= $form->field($model, 'category')->dropDownList(Pages_model::getCategoriesByType($model->type), ['class' => 'short form-control'])->label(false) ?>
                <?else:?>
                    <?=Html::dropDownList('', null, ['Выберите категорию'], ['class' => 'form-control', 'disabled'=>'disabled'])?>
                <?endif?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
